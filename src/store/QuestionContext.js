import React from "react";

const QuestionContext=React.createContext({
    questions:[],
    totalQuestionNumber:0,
    addQuestion:(question)=>{},
    removeQuestion:(id)=>{}
});

export default QuestionContext;