import React, { useReducer } from "react";
import QuestionContext from "./QuestionContext";

const defaultQuestionState={
    questions:[],
    totalQuestionNumber:0,
};
const questionResducer=(state,action)=>{
    if(action.type==='ADD'){
        const QuestionsIndex=state.questions.findIndex(
            (question)=>question.id===action.question.id
        );

        const UpdatedQuestions = state.questions[QuestionsIndex];
        let updateQuestions;
        if(UpdatedQuestions){
            const updateQuestion={
                ...UpdatedQuestions,
                selectedOption:action.question.selectedOption
            }
            updateQuestions=[...state.questions];
            updateQuestions[QuestionsIndex]=updateQuestion
        }
        else{updateQuestions=state.questions.concat(action.question)
        }
        return{
            questions:updateQuestions,
            totalQuestionNumber:updateQuestions.length
        }
    }
    
    if(action.type==='REMOVE'){
        const QuestionsIndex=state.questions.findIndex(
            (question)=>question.id===action.id
        );
        let UpdatedQuestions = state.questions[QuestionsIndex];
        let updateQuestions;
        if(UpdatedQuestions){
            updateQuestions=state.questions.filter((question)=>question.id!==action.id);

        }else{
            updateQuestions=[...state.questions];
        }
        return{
            questions:updateQuestions,
            totalQuestionNumber:updateQuestions.length
        }
    }
    return defaultQuestionState
}

const QuestionProvider=(props)=>{
    const [currentState,dispatchQuestionAction]=useReducer(questionResducer,defaultQuestionState);
    const addItemtoWrongHandler=(question)=>{
        dispatchQuestionAction({type:'ADD',question:question});
    }

    const removeQuestionFromWrongHandler=(id)=>{
        dispatchQuestionAction({type:'REMOVE',id:id});
    }

    const questionContext={
        questions:currentState.questions,
        totalQuestionNumber:currentState.totalQuestionNumber,
        addQuestion:addItemtoWrongHandler,
        removeQuestion:removeQuestionFromWrongHandler,
    }

    return(
        <QuestionContext.Provider value={questionContext}>
            {props.children}
        </QuestionContext.Provider>
    )
}

export default QuestionProvider;

