import React, { useState, useEffect, useContext } from 'react';
import './App.css';
import Login from './components/Login/Login';
import MainHeader from './components/MainHeader/MainHeader';
import AddUser from './components/Users/RegisterForm';
// import UserList from './components/Users/UserList';
import NewQuestion from './components/QuizeApp/NewQuestionComponent';
import QuestionandOption from './components/QuizeApp/QuestionandOptions';
import SideBar from './components/SideBar';
import TraineeUser from './components/TraineeUser/Trainee';
import Card from './components/UI/Card/Card';
import AddAdmin from './components/Admin/AdminComponent';
import QuestionProvider from './store/QuestionProvider';

import QuestionContext from './store/QuestionContext';
import { Routes } from 'react-router-dom';
import CartModule from './components/Cart/QuestionModelCart';

const trainee_users = [
  {
    id: 1,
    userName: 'Pankaj',
    email: 'pankaj@hcl.com',
    password: 'pankajsharma',
    confirmPassword: 'pankajsharma',
    phoneNumber: '0998765656',
    role: 'trainer'
  },

  {
    id: 2,
    userName: 'Abhilasha',
    email: 'abhilasha@hcl.com',
    password: 'abhilasha',
    confirmPassword: 'abhilasha',
    phoneNumber: '7899210332',
    role: 'trainee'
  },

  {
    id: 3,
    userName: 'Bagya',
    email: 'bhagya@hcl.com',
    password: 'bhagyalakshmi',
    confirmPassword: 'bhagyalakshmi',
    phoneNumber: '7899210332',
    role: 'admin'
  }
]


// if((email=='admin@hcl.com')&&(pass=='admin')){
//   // login heere
// }

const initial_questions = [
  {
    id: 1,
    text: 'What is the capital of the India',
    category: 'javascrpit',
    option1: 'New York',
    option2: 'New Delhi',
    option3: 'Bangalore',
    option4: 'Bangalore',
    correctAns: 'New Delhi',

  },

  {
    id: 2,
    text: 'The correct sequence of HTML tags for starting a webpage is :',
    category: 'html',
    option1: 'Head, Title, HTML, body',
    option2: 'none of the above',
    option3: 'HTML, Head, Title, Body',
    option4: 'Head, Title, body, script',
    correctAns: 'HTML, Head, Title, Body'

  },
  {
    id: 3,
    text: 'How can we write comments in CSS? :',
    category: 'css',
    option1: '/* */',
    option2: '%',
    option3: '/',
    option4: 'None',
    correctAns: '/* */'
  },
  {
    id: 4,
    text: 'Can negative values be allowed in padding property? :',
    category: 'javascript',
    option1: 'Yes',
    option2: 'No',
    option3: 'Depends on Property',
    option4: 'None of the above',
    correctAns: 'No'

  },


  {
    id: 5,
    text: 'What is JavaScript? ',
    category: 'javascript',
    option1: 'Only Client side program language',
    option2: 'Server side program language',
    option3: 'Both Server side and Client side program language',
    option4: 'None of the above',
    correctAns: 'Both Server side and Client side program language'

  },

  {
    id: 6,
    text: 'Which type of JavaScript language is ___',
    category: 'javascript',
    option1: 'Object-Oriente',
    option2: 'Object-Based',
    option3: 'Assembly-language',
    option4: 'High-level',
    correctAns: 'Object-Based'

  },
  {
    id: 7,
    text: 'In JavaScript, what is a block of statement?',
    category: 'javascript',
    option1: 'Conditional block',
    option2: 'both conditional block and a single statement',
    option3: 'block that contains a single statement',
    option4: 'block that combines a number of statements into a single compound statement',
    correctAns: 'both conditional block and a single statement'
  },


  {
    id: 8,
    text: 'How can we select an element with a specific ID in CSS?',
    category: 'css',
    option1: '#',
    option2: '%',
    option3: '/',
    option4: 'None',
    correctAns: '#'

  },
  {
    id: 9,
    text: 'The property in CSS used to change the text color of an element is :',
    category: 'css',
    option1: 'bgcolor',
    option2: 'color',
    option3: 'background-color',
    option4: 'None of These',
    correctAns: 'color'
  },
  {
    id: 10,
    text: 'The property in CSS used to change the background color of an element is :',
    category: 'css',
    option1: 'bgcolor',
    option2: 'color',
    option3: 'background-color',
    option4: 'None of These',
    correctAns: 'background-color'

  },
  {
    id: 11,
    text: 'CSS Stands for- :',
    category: 'css',
    option1: 'Cascading Style Sheet',
    option2: 'Cascade sheet style',
    option3: 'Cascade styleing sheet',
    option4: 'None of These',
    correctAns: 'Cascading Style Sheet'

  },

];

// localStorage.setItem('totlat_user',JSON.stringify(trainee_users))
// console.log('trainee_users :>> ', trainee_users);


function App() {
  const [isLoggedInAdmin, setIsLoggedInAdmin] = useState(false);
  const [isLoggedInTrainee, setIsLoggedInTrainee] = useState(false);
  const [isLoggedInTrainer, setIsLoggedInTrainer] = useState(false);
  const [question, setQuestion] = useState(initial_questions);
  const [users, setUsersList] = useState(trainee_users);
  const [isRegister,setIsRegister]=useState(false);
  const [isCartShown,setIsCartShown]=useState(false);

  const addQuestionHandler = (question) => {
    // console.log('Inside The App Component');
    setQuestion((prevQuestion) => {
      return [question, ...prevQuestion]
    })
  }

  const onAddUserHandler = (users) => {
    setUsersList((prevState) => {
      return [users, ...prevState]
    })
  }
  // users.map((user) => {
  //   return (user.email, user.password)
  // })


  // useEffect(()=>{
  //   if((email===users.email)&& (password===users.password)){
  //     if(users.role==='tariner'){
  //       setIsLoggedInTrainer(true);
  //     }
  //     if(users.role==='tarinee'){
  //       setIsLoggedInTrainee(true);
  //     }
  //   }
  // },[isLoggedInTrainee,isLoggedInTrainer])


  useEffect(() => {

    users.filter((user)=>{
      const loggegInUserTrainerStatus = JSON.parse(localStorage.getItem('isLoggedInTrainer'));
      const loggegInUserTraineeStatus = JSON.parse(localStorage.getItem('isLoggedInTrainee'));
      const loggegInUserAdminStatus = JSON.parse(localStorage.getItem('isLoggedInAdmin'));
      // console.log('loggegInUserTrainerStatus :>> ', loggegInUserTrainerStatus);
      // console.log('loggegInUserTraineeStatus :>> ', loggegInUserTraineeStatus);
      if (loggegInUserTrainerStatus === 'trainer') {
        setIsLoggedInTrainer(true);
      } else if (loggegInUserTraineeStatus === 'trainee') {
        setIsLoggedInTrainee(true);
      } else if (loggegInUserAdminStatus === 'admin') {
        setIsLoggedInAdmin(true);
      }
    })
   
  }, [isLoggedInTrainer, isLoggedInTrainee, isLoggedInAdmin])


  const loginHandlerTrainer = (email, password) => {
    users.filter((user) => {
      // console.log('user :>> ', user);
      if (email === user.email && password === user.password) {
        if (user.role === 'trainer') {
          // localStorage.setItem('isLoggedInTrainer', 'trainer')
          localStorage.setItem('isLoggedInTrainer',JSON.stringify(user));
          setIsLoggedInTrainer(true);
        }
        if (user.role === 'trainee') {
          // localStorage.setItem('isLoggedInTrainee', 'trainee');
          localStorage.setItem('isLoggedInTrainee',JSON.stringify(user));
          setIsLoggedInTrainee(true);
         
         
        }
        if (user.role === 'admin') {
          // localStorage.setItem('isLoggedInAdmin', 'admin')
          localStorage.setItem('isLoggedInAdmin',JSON.stringify(user));
          setIsLoggedInAdmin(true);
        }
      }
    });
  };


  const loginHanlderAdmin=(email,password)=>{
    if((email==='bhagyalakshmi@hcl.com')&&(password==='bhagyalakshmi')){
      localStorage.setItem('isLoggedInAdmin','admin');
          setIsLoggedInAdmin(true);
    }
  }


  const logoutHandlerTrainer = () => {
    // localStorage.removeItem('isLoggedInTrainer', 'trainer');
    users.filter((user)=>{
      localStorage.removeItem('isLoggedInTrainer',JSON.stringify(user));
    })
    setIsLoggedInTrainer(false);
  };

  const loginTrainerHandlerAddForm = () => {
    setIsLoggedInTrainer(true);
  }
  const loginRegisterForm=()=>{
    setIsRegister(true);
    setIsRegister(false);

  }
  const signUpCancel=()=>{
    setIsRegister(false);
  }

  const logoutHandlerTrainee = () => {
    // localStorage.removeItem('isLoggedInTrainee', 'trainee');
    users.filter((user)=>{
      localStorage.removeItem('isLoggedInTrainee',JSON.stringify(user));
    })
    setIsLoggedInTrainee(false);
  };
  const loginTraineeHandlerAddForm = () => {
    setIsLoggedInTrainee(true);
  }

  const logoutHandlerAdmin = () => {
    // localStorage.removeItem('isLoggedInAdmin', 'admin');

    users.filter((user)=>{
      localStorage.removeItem('isLoggedInAdmin',JSON.stringify(user));
    })
    setIsShowAttept(false);
    setIsLoggedInAdmin(false)
  }

  const loginAdminHandlerAddForm = () => {
    setIsLoggedInAdmin(true);
  }

  const signUpTrainer=()=>{
    setIsRegister(true);
  }

  const showCartHandler=()=>{
    setIsCartShown(true);

  }
    const closeCartHandler=()=>{
      setIsShowFinalResult(false);
      setIsCartShown(false);
      // setIsShowAttept(false);
    }
    
    const [disable,setIsDisable]=useState(false);
    const [isShowAttempt, setIsShowAttept] = useState(false);
    const [isShowFinalResult,setIsShowFinalResult]=useState(false);
    // const crtquestion=useContext(QuestionContext)
    // const hashItem=crtquestion.questions.length>0;
    const startTestHandler=()=>{
      setIsShowFinalResult(false);
      setIsCartShown(false);
      setIsShowAttept(false);
    }
    const submitHandler=()=>{
      setIsShowFinalResult(true);
      setIsShowAttept(true);
      setIsDisable(true);
    }
    const cancelHandler=()=>{
      setUsersList(false);
      // setIsShowAttept(false);
    }
    const showWrongAttempts=()=>{
      setIsShowFinalResult(false);
      setIsCartShown(false);
      setIsShowAttept(true);
    }
  return (
    <QuestionProvider>
      <Card>
        {isCartShown&&<CartModule onCloseCart={closeCartHandler}/>}
      
        <div id="outer-container">
          <MainHeader
            // users={users}
            isAuthenticatedTrainer={isLoggedInTrainer}
            isAuthenticatedTrainee={isLoggedInTrainee}
            isAuthenticatedAdmin={isLoggedInAdmin}
            // isAuthenticatedSignup={isRegister}
            onLogoutTrainer={logoutHandlerTrainer}
            onLogoutTrainee={logoutHandlerTrainee}
            onLogoutAdmin={logoutHandlerAdmin}
            onSignUpTrainer={signUpTrainer}
            onShowCart={showCartHandler}
            isShowAttempt={isShowAttempt}
          />
     
          <main>
            {!isLoggedInTrainer && !isLoggedInAdmin && !isLoggedInTrainee && !isRegister &&
              (<div> 
                <Login users={users} onLoginTrainer={loginHandlerTrainer} onLoginAdmin={loginHanlderAdmin} />
                {/* <AddUser onAddUsers={onAddUserHandler} /> */}
              </div>)}

              {isRegister&&
              (<div>
                 <AddUser users={users} onAddUsers={onAddUserHandler} onClick={loginRegisterForm} onCancel={signUpCancel} />
              </div>)
               }
            {isLoggedInTrainer &&
              (<div id="page-wrap">
                <SideBar pageWrapId={'page-wrap'} outerContainerId={'outer-container'} onLogoutTrainer={logoutHandlerTrainer} />
                <NewQuestion onAddQuestion={addQuestionHandler} onClick={loginTrainerHandlerAddForm} />
                <QuestionandOption questions={question} />  </div>)}
            {isLoggedInTrainee && 
              (<div id="page-wrap">
                <SideBar pageWrapId={'page-wrap'} outerContainerId={'outer-container'} onLogoutTrainee={logoutHandlerTrainee} />
               
                <QuestionandOption   onStarttest={startTestHandler}  onsubmit={submitHandler} onCancel={cancelHandler}  questions={question} />
                {/* <TraineeUser questions={question} onClick={loginTraineeHandlerAddForm} /> */}
              </div>)}

            {isLoggedInAdmin && (<div> <SideBar pageWrapId={'page-wrap'} outerContainerId={'outer-container'} onLogoutAdmin={logoutHandlerAdmin} />
              <AddAdmin onAddUsers={onAddUserHandler} users={users} onClick={loginAdminHandlerAddForm} />
            </div>)}
          </main>
        </div>
      </Card>

    </QuestionProvider>
  );
}

export default App;
