import React from "react";
import './SideBar.css'
import { slide as Menu } from 'react-burger-menu';

const SideBar = (props) => {
    return (
        <Menu className="menu-bar">
            <a className="menu-item" href="/dashboard">
                Dashborad
            </a>
            <a className="menu-item" href="/profile">
                Profile
            </a>
            <a className="menu-item" href="/changepassword">
                Change Password
            </a>
            {/* <a className="menu-item" href="/" 
            onClick={props.onLogoutTrainee}  >Logout</a> */}
        </Menu>
    )
}

export default SideBar;

