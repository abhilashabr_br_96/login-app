import React, { useState } from "react";
import './Trainee.css'
import QuizeTimer from "../QuizeApp/QuizeTimer";
import Card from "../UI/Card/Card";
import DownloadFile from "../DownloadFile";
import Button from "../UI/Button/Button";
const TraineeUser = (props) => {

    const [showFinalResults, setFinalResults] = useState(false);

    const [score, setScore] = useState(0);

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [isActive, setisActive] = useState(0);

    const questions = [
        {
            text: 'What is the capital of the Karnatak',
            options: [
                { id: 0, text: 'New York', isCurrect: false },
                { id: 1, text: 'Boston', isCurrect: false },
                { id: 2, text: 'Santha FFe', isCurrect: false },
                { id: 3, text: 'Washington DC', isCurrect: true }
            ]
        },

        {
            text: 'The correct sequence of HTML tags for starting a webpage is :',
            options: [
                { id: 0, text: 'Head, Title, HTML, body', isCurrect: false },
                { id: 1, text: 'nonw of the above', isCurrect: false },
                { id: 2, text: 'HTML, Head, Title, Body', isCurrect: true },
                { id: 3, text: 'Head, Title, body, script', isCurrect: false }
            ]
        },
        {

            text: 'How can we write comments in CSS? :',
            options: [
                { id: 0, text: '/* */', isCurrect: true },
                { id: 1, text: '//', isCurrect: false },
                { id: 2, text: '#', isCurrect: false },
                { id: 3, text: '/', isCurrect: false }
            ]
        },
        {

            text: 'Can negative values be allowed in padding property? :',
            options: [
                { id: 0, text: 'Yes', isCurrect: true },
                { id: 1, text: 'No', isCurrect: false },
                { id: 2, text: 'Depends on Property', isCurrect: false },
                { id: 3, text: 'None of the above', isCurrect: false }
            ]
        },


        {

            text: 'What is JavaScript? :',
            options: [
                { id: 0, text: 'Only Client side program language', isCurrect: false },
                { id: 1, text: 'Server side program language', isCurrect: false },
                { id: 2, text: 'Both Server side and Client side program language', isCurrect: true },
                { id: 3, text: 'None of the above', isCurrect: false }
            ]
        },

        {

            text: 'Which type of JavaScript language is ___',
            options: [
                { id: 0, text: 'Object-Oriented', isCurrect: false },
                { id: 1, text: 'Object-Based', isCurrect: true },
                { id: 2, text: 'Assembly-language', isCurrect: false },
                { id: 3, text: 'High-level', isCurrect: false }
            ]
        },
        {

            text: 'In JavaScript, what is a block of statement?',
            options: [
                { id: 0, text: 'Conditional block', isCurrect: false },
                { id: 1, text: 'both conditional block and a single statement', isCurrect: true },
                { id: 2, text: 'block that contains a single statement', isCurrect: false },
                { id: 3, text: 'block that combines a number of statements into a single compound statement', isCurrect: false }
            ]
        },


        {

            text: 'How can we select an element with a specific ID in CSS?',
            options: [
                { id: 0, text: '#', isCurrect: true },
                { id: 1, text: '.', isCurrect: false },
                { id: 2, text: '/', isCurrect: false },
                { id: 3, text: 'None of These', isCurrect: false }
            ]
        },
        {
            text: 'The property in CSS used to change the background color of an element is :',
            options: [
                { id: 0, text: 'bgcolor', isCurrect: false },
                { id: 1, text: 'color', isCurrect: false },
                { id: 2, text: 'background-color', isCurrect: true },
                { id: 3, text: 'None of These', isCurrect: false }
            ]
        },
        {

            text: 'The property in CSS used to change the text color of an element is :',
            options: [
                { id: 0, text: 'bgcolor', isCurrect: false },
                { id: 1, text: 'color', isCurrect: true },
                { id: 2, text: 'background-color', isCurrect: false },
                { id: 3, text: 'None of These', isCurrect: false }
            ]
        },

    ];

    console.log('correct ans :>> ', props.questions);

    {
        props.questions.map((question) => {
            console.log('question.correctAns :>> ', question.correctAns);
        })
    }

    const optionClicked = (isCurrect) => {
        // startTimerLine(widthValue);
        // clearInterval(counterLine);
        // isCurrect.currentOption.style.backgroundColor = 'green';
        // isCurrect.currentQuestion.style.color = 'white';
        // isCurrect.currentQuestion.classList.add('my-class-1', 'mu-class-2');
        if (isCurrect) {
            setScore(score + 1);
        }

        if (currentQuestion + 1 < questions.length) {
            setCurrentQuestion(currentQuestion + 1);
        } else {
            setFinalResults(true);
        }

    }
    const restartGame = () => {
        setScore(0);
        setCurrentQuestion(0);
        setFinalResults(false);
    }




    return (
        <React.Fragment>
            <Card className="App">
                {/* 1. Header */}
                <div className="headline">
                    Quize Test
                </div>
                <div className="header-timer">
                    <div><QuizeTimer /> </div>
                </div>


                <div className="header-score">
                    <h3>Current Score : {score}</h3>
                    {/* <h1>{questions}</h1> */}
                </div>

                {/* 2. Current Score */}

                <div className="quize-lsit">
                    {showFinalResults ?
                        /* 4.Final Results */
                        // isCurrect

                        <div className='final-results '>
                            <h1>Final Results</h1>
                            <div>

                            </div>
                            <h3>{score} out of {questions.length} correct - ({score / questions.length * 100}%)</h3>
                            <button onClick={() => restartGame()}>Restart Game</button>
                            <DownloadFile />

                        </div>
                        :
                        /* 3. Question Card */

                        <div className='question-card'>

                            <h3>Question {currentQuestion + 1} out of {questions.length}</h3>

                            <div class="time_line">timer Load</div>
                            <h4 className='question-text'>{questions[currentQuestion].text}</h4>

                            <ul>
                                {/* style={{backgroundColor:isActive ?'green':'red',color:isActive?'white':''}} */}

                                {questions[currentQuestion].options.map((option) => {
                                    return (
                                        <li
                                            onClick={() => optionClicked(option.isCurrect)}
                                            key={option.id} style={{ backgroundColor: option.isCurrect ? '#075d3d' : '#9d0a23', color: option.isCurrect ? 'white' : '' }}>{option.text}</li>
                                    )

                                })}

                            </ul>

                        </div>
                    }
                </div>
                <div className="show_button">
                <Button type='prev'> Previous</Button>
                <Button type='next'> Next</Button>
            </div>
            </Card>
            
        </React.Fragment>
    )

}

export default TraineeUser;
