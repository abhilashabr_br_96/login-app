import React, {useState} from "react";
import Card from "../UI/Card/Card";
import UserList from "../Users/UserList";
import Button from "../UI/Button/Button";
import AddTrainer from "./AddTrainer";
import classes from "./AddTrainer.module.css";
const AddAdmin = (props) => {
    const [isEditTrainer,setisEditTrainer]=useState(false);
    const startEditingHandler=()=>{
        setisEditTrainer(true);
    }
    const stopEditingHandler=()=>{
        setisEditTrainer(false);
    }
   
    return (
        <Card>
            <div>{!isEditTrainer && (
                <div >
                    <Button className={classes.button_add} onClick={startEditingHandler}>Add New Trainer</Button>
                    <UserList users={props.users} />
                </div>
            )}
                {isEditTrainer && <AddTrainer onAddUsers={props.onAddUserHandler} isEditTrainer={isEditTrainer} onCancel={stopEditingHandler}/>}

            </div>




        </Card>
    )
}
export default AddAdmin;