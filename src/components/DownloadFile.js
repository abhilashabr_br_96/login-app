import React from "react";
import Button from "./UI/Button/Button";
import classes from './DownloadFile.module.css';

const DownloadFile = () => {
    return (
        <div >
            {/* <Button className={classes.download}> */}
            <a className={classes.download}
                href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                download
            >
                Click to download Certificate
            </a>
            {/* </Button> */}

        </div>
    )
}

export default DownloadFile;