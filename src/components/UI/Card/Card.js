import React from 'react';
import classes from './Card.module.css';
// import image from '../../../assets/image5.png';
import '../../../App.css';
const Card = (props) => {
  return (
    <div>
      {/* <div className="App">
          <header className="App-header ">
            <img src={image} className="App-logo" alt="image" />
          </header>
        </div> */}
      <div className={`${classes.card} ${props.className}`}>
        
        {props.children}
      </div>
    </div>
  );
};

export default Card;
