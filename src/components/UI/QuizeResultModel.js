import React from "react";
// import classes from './Model.model.css';
import ReactDOM from "react-dom";
// import classes from './StartQuizeModel.module.css';

const Backdrop = (props) => {
    return <div className='backdrop' onClick={props.onClick}></div>
}

const ModalOverLay = (props) => {
    return (
        <div className='modal'>
            <div className='header'>
                <div className='content'>{props.children}</div>
            </div>
        </div>
    );
};

const QuizeResultModel = (props) => {
    return (
        <React.Fragment>
            {ReactDOM.createPortal(<Backdrop onClick={props.onClick}/>, document.getElementById('backdrop-root'))}
            {ReactDOM.createPortal(<ModalOverLay>{props.children}</ModalOverLay>, document.getElementById('overlay-root'))}
        </React.Fragment>
    )
}

export default  QuizeResultModel;