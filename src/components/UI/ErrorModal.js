import React from "react";
import classes from './ErrorModal.module.css';
import ReactDOM from "react-dom";

const Backdrop = (props) => {
    return <div className={classes.backdrop} onClick={props.onConfirm}></div>
}

const ModalOverLay = (props) => {
    return (
        <div className={classes.modal}>
            <header className={classes.header}>
                <h3>{props.title}</h3>
            </header>
            <div className={classes.content}>
                {props.message}
            </div>
            <footer className={classes.footer}>
                <button onClick={props.onConfirm}>OK</button>
            </footer>
        </div>
    )
}

const ErrorModal = (props) => {
    return (
        <React.Fragment>
            {ReactDOM.createPortal(<Backdrop onConfirm={props.onConfirm} />, document.getElementById('backdrop-root'))}
            {ReactDOM.createPortal(<ModalOverLay title={props.title} message={props.message} onConfirm={props.onConfirm} />, document.getElementById('overlay-root'))}
        </React.Fragment>
    )
}

export default ErrorModal;