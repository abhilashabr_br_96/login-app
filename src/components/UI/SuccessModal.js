import React from "react";
import classes from './SuccessModal.module.css';
import Button from "./Button/Button";
import ReactDOM from "react-dom";

const Backdrop = (props) => {
    return <div className={classes.success_backdrop} onClick={props.onConfirm}></div>
}

const ModalOverLay = (props) => {
    return (
        <div className={classes.success_backdrop_modal}>
            <header className={classes.success_backdrop_header}>
                <h3>{props.title}</h3>
            </header>
            <div className={classes.success_backdrop_content}>
                {props.message}
            </div>
            <footer className={classes.success_backdrop_footer}>
                <button onClick={props.onConfirm}>OK</button>
            </footer>
        </div>
    )
}

const SuccessModal = (props) => {
    return (
        <React.Fragment>
            {ReactDOM.createPortal(<Backdrop onConfirm={props.onConfirm} />, document.getElementById('backdrop-root'))}
            {ReactDOM.createPortal(<ModalOverLay title={props.title} message={props.message} onConfirm={props.onConfirm} />, document.getElementById('overlay-root'))}
        </React.Fragment>
    )
}

export default SuccessModal;