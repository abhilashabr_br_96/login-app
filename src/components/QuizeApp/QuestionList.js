import React, { useContext, useState } from "react";
import './QuestionList.css';
import QuizeApp from "./QuizeAppComponent";
import QuestionContext from "../../store/QuestionContext";
import Button from "../UI/Button/Button";
import Card from "../UI/Card/Card";
import ResultQuizeModule from "../Cart/ResultCart";
import CartModule from "../Cart/QuestionModelCart";

const QuestionList = (props) => {
    const [isCartShown, setisCartShown] = useState(false);
    const [isShowAttempt,setShowAttepmt]=useState(null);
    const [disable,setIsDisable]=useState(true);
    const [isShowFinalResult, setIsShowFinalResult] = useState(false);
    const crtquestion = useContext(QuestionContext);
    const closeCartHandler = () => {
        setIsShowFinalResult(false);
        setisCartShown(false);
        // setIsShowAttept(false);
    }
    const startTestHandler = () => {
        setIsShowFinalResult(false);
        // setIsCartShown(false);
        // setIsShowAttept(false);
    }
    const showWrongAttempts = () => {
        setIsShowFinalResult(false);
        // setIsCartShown(false);
        // setIsShowAttept(true);
    }


    const submitHandler = () => {
        console.log('optionValue :>> ', props.optionValue);
        setIsDisable(false)
        setShowAttepmt(props.onsubmit)
        setIsShowFinalResult(true);
        setisCartShown(false);
        // setIsDisable(true);
    }
    const selecteQuestinHandler = (selectedPrevQuestion, selectedOption) => {
        if (selectedPrevQuestion.correctAns !== selectedOption) {
            crtquestion.addQuestion({
                id: selectedPrevQuestion.id,
                text: selectedPrevQuestion.text,
                option1: selectedPrevQuestion.option1,
                option2: selectedPrevQuestion.option2,
                option3: selectedPrevQuestion.option3,
                option4: selectedPrevQuestion.option4,
                correctAns: selectedPrevQuestion.correctAns,
                selectedOption: selectedOption
            });
        }
        else {
            crtquestion.removeQuestion(selectedPrevQuestion.id)
        }

    }


    if (props.questions.length === 0) {
        return (
            <h1 className="question-list_err">There is no Data</h1>
        )
    }
    {console.log('optionValue :>> ', props.optionValue)}
    return (
        <Card>
            {isCartShown&&<CartModule onClick={closeCartHandler}/>}
            {isShowFinalResult &&
                <ResultQuizeModule
                selectedFilteredQuestionLength={props.selectedFilteredQuestionLength}
                selectedPrevQuestion={props.selectedPrevQuestion}
                selectedOption={props.selectedOption}
                    selected={props.selected}
                    onClick={props.onStarttest}
                    onCloseCart={closeCartHandler}
                    question={props.questions} />}
            <ul className="question-list">
                {props.questions.map((question, index) => {
                    // console.log('question.questionId :>> ', question.id);

                    return <QuizeApp
                        optionValue={props.optionsValue}
                        key={question.id}
                        onQuestionSelected={selecteQuestinHandler}
                        questions={props.questions}
                        index={index + 1} question={question} />
                })}
            </ul>
            <div className="show-Button">
                <div><Button disabled={disable} onClick={submitHandler}>Submit</Button></div>
                {/* <div><Button className="cancel-add-btn" onClick={props.onCloseCart}>Cancel</Button></div> */}
            </div>
            {/* {props.questions.length} */}
            {/* <div className='question-attempted'>{props.index} out of {props.questions.length} </div> */}
            {console.log('props.optionsValue :>> ', props.optionsValue)}
        </Card>
    )
}
export default QuestionList;