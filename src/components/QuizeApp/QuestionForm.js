import React, { useState } from "react";
import './QuestionForm.css';
// import SelectMultiple from "./CategoryComponent";'


const QuestionForm = (props) => {
    const [enteredCategory, setEnteredCategory] = useState('');
    const [enteredQUestion, setEnteredQuestion] = useState('');
    const [enteredOption1, setEnteredOption1] = useState('');
    const [enteredOption2, setEnteredOption2] = useState('');
    const [enteredOption3, setEnteredOption3] = useState('');
    const [enteredOption4, setEnteredOption4] = useState('');
    const [enteredCorrectAnswer, setEnteredCorrectAnswer] = useState('');
    const [isValid, setIsValid] = useState(true);

    const enteredCategoryHandler = (event) => {
        if (event.target.value.trim.length > 0) {
            setIsValid(true);
        }
        console.log('object :>> ', event.target.value);
        setEnteredCategory(event.target.value)
    }

    const questionChangeHandler = (event) => {
        if (event.target.value.trim().length > 0) {
            setIsValid(true);
        }
        setEnteredQuestion(event.target.value)
    };

    const option1ChangeHandler = (event) => {
        if (event.target.value.trim().length > 0) {
            setIsValid(true);
        }
        setEnteredOption1(event.target.value);
    };

    const option2ChangeHandler = (event) => {
        if (event.target.value.trim().length > 0) {
            setIsValid(true);
        }
        setEnteredOption2(event.target.value);
    };
    const option3ChangeHandler = (event) => {
        if (event.target.value.trim().length > 0) {
            setIsValid(true);
        }
        setEnteredOption3(event.target.value);
    };

    const option4ChangeHandler = (event) => {
        if (event.target.value.trim().length > 0) {
            setIsValid(true);
        }
        setEnteredOption4(event.target.value);
    };

    const correctoptionHandler = (event) => {
        if (event.target.value.trim().length > 0) {
            setIsValid(true);
        }
        setEnteredCorrectAnswer(event.target.value);
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        // console.log('form Submiteed');

        const newQuestions = {

            text: enteredQUestion,
            category: enteredCategory,
            // options: [
            //     enteredOption1,
            //     enteredOption2,
            //     enteredOption3,
            //     enteredOption4
            // ],
            option1: enteredOption1,
            option2: enteredOption2,
            option3: enteredOption3,
            option4: enteredOption4,
            correctAns: enteredCorrectAnswer,
        }

        if (enteredCategory.trim().length === 0
            || enteredOption1.trim().length === 0
            || enteredOption2.trim().length === 0
            || enteredOption3.trim().length === 0
            || enteredOption4.trim().length === 0
            || enteredCorrectAnswer.trim().length === 0) {
            setIsValid(false);
            return;
        }
        props.onSaveQuestionseData(newQuestions);
        console.log('newQuestions :>> ', newQuestions);
        setEnteredQuestion('');
        setEnteredOption1('');
        setEnteredOption2('');
        setEnteredOption3('');
        setEnteredOption4('');
        setEnteredCategory('');
        setEnteredCorrectAnswer('');


    }

    return (
        <React.Fragment>
            <form onSubmit={submitFormHandler}>
                <div className="heading"><h1>Add New Question</h1></div>

                <div className={`form-control new-question_controls ${!isValid ? 'invalid' : ''}`}>

                    <div>
                        <div className="">
                            <label>Category</label>
                            <select type='text' required value={props.selectedCategory}
                                onChange={enteredCategoryHandler} className='select-option'>
                                <option value=''>Select the Category</option>
                                <option value='javascript'>Javascript</option>
                                <option value='html'>HTML</option>
                                <option value='css'>CSS</option>
                                <option value='react'>ReactJs</option>
                                <option value='angular'>Angular</option>
                            </select>
                        </div>

                        <div className={` new-question__control ${!enteredQUestion.trim().length ? "" : "noinvaild"} `}>
                            <label>Question</label>
                            <input type='text' placeholder='Enter Question' value={enteredQUestion} onChange={questionChangeHandler} />
                        </div>

                        <div className="new-option__control input">
                            <lable>Option A.</lable>
                            <input value={enteredOption1} className={` ${!enteredOption1.trim().length ? "" : "noinvaild"}  `} placeholder='Enter option A' onChange={option1ChangeHandler}></input>

                            <lable>Option B.</lable>
                            <input value={enteredOption2} className={`${!enteredOption2.trim().length ? "" : "noinvaild"} `} placeholder='Enter option B' onChange={option2ChangeHandler}></input>

                            <lable>Option C.</lable>
                            <input value={enteredOption3} className={` ${!enteredOption3.trim().length ? "" : "noinvaild"} `} placeholder='Enter option C' onChange={option3ChangeHandler}></input>

                            <lable>Option D.</lable>
                            <input value={enteredOption4} className={` ${!enteredOption4.trim().length ? "" : "noinvaild"} `} placeholder='Enter option D' onChange={option4ChangeHandler}></input>
                        </div>

                        <div className="new-questions__actions">
                            <label>Corect Answer</label>
                            <select type='text' className={` select-option ${!enteredCorrectAnswer.trim().length ? "" : "noinvaild"} `} value={enteredCorrectAnswer} onChange={correctoptionHandler}>
                                <option value=''>Select the Correct Answer</option>
                                <option value={enteredOption1}>option A</option>
                                <option value={enteredOption2}>option B</option>
                                <option value={enteredOption3}>option C</option>
                                <option value={enteredOption4}>option D</option>

                            </select>
                        </div>

                    </div>

                    <div className="add_cancel_actions">
                        <div><button className="add-question-btn">Add New Question</button></div>
                        <div><button className="cancel-add-btn" onClick={props.onCancel}>Cancel</button></div>
                    </div>
                </div>
            </form>
            <div>
                {/* <QuestionList questions={props.questions}/> */}
            </div>
        </React.Fragment>
    )
}

export default QuestionForm;
