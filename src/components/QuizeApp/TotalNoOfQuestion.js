import React from "react";

const TotalNoQuestion = (props) => {
    const { numberofQuestionLength } = props;
    return (
        <div>
            <div><p>Total Question :</p>{numberofQuestionLength.length}</div>
        </div>
    )
}

export default TotalNoQuestion;