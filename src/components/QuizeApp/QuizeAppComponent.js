import './QuizeAppComponent.css';
import React, { useState } from 'react';
import Card from '../UI/Card/Card';
import { setSelectionRange } from '@testing-library/user-event/dist/utils';

const QuizeApp = (props) => {
    const { question } = props;
    const [score, setScore] = useState(0);
    const [showFinalResults, setFinalResults] = useState(false);
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [isChecked,setIsChecked]=useState(null);

    const optionClicked = (event) => {
        setIsChecked(event.currentTarget.getAttribute("value"))
        let optionsValue= event.currentTarget.getAttribute("value");
        console.log('question,optionsValue :>> ',optionsValue);
        props.onQuestionSelected(question,optionsValue);
        console.log('props.questions.length :>> ', props.questions.length);
       
    }
    const restartGame = () => {
        setScore(0);
        setCurrentQuestion(0);
        setFinalResults(false);
    }


    return (
        <React.Fragment>
        <Card className=''>
            <div className='card'>
                <div className='question-text'><h4>{props.index}.{question.text}</h4></div>
                <div>
                    {/* <div>{score}</div> */}
                    <div className='currect-answer'>
                        <div>
                            <ul>
                                <li onClick={optionClicked} className={`${isChecked===question.option1?'blue':''}`} value={question.option1}>{question.option1}</li> 
                                <li onClick={optionClicked} className={`${isChecked===question.option2?'blue':''}`} value={question.option2}>{question.option2}</li>
                                <li onClick={optionClicked} className={`${isChecked===question.option3?'blue':''}`} value={question.option3}>{question.option3}</li>
                                <li onClick={optionClicked} className={`${isChecked===question.option4?'blue':''}`} value={question.option4}>{question.option4}</li>
                            </ul>
                        </div>
                    </div>

                    <div className='question-attempted'>{props.index} out of {props.questions.length} </div>
                </div>
            </div>
        </Card>
        </React.Fragment>
    )
}

export default QuizeApp;
