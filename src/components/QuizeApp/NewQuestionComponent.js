import React, { useState } from "react";
import './NewQuestionComponent.css';
import QuestionForm from "./QuestionForm";

const NewQuestion=(props)=>{
    const [isEditable,setIsEditable]=useState(false);

    const saveQuestionDtataHanadler=(enteredQuestion)=>{
        const questionData={
            ...enteredQuestion,
            id:Math.floor().toString(),
        };
        props.onAddQuestion(questionData)
        setIsEditable(true);
    }
        const startEditingHandler=()=>{
            setIsEditable(true);
        }
        const stopEditingHandler=()=>{
            setIsEditable(false);
        }
    return(
        <div className="new-question">

        {!isEditable && (<div><button onClick={startEditingHandler}>Add New Questions</button></div>)}

        {isEditable &&
        (<div><QuestionForm onSaveQuestionseData={saveQuestionDtataHanadler} onCancel={stopEditingHandler}/> 
        </div>)
         }
            
        </div>
    )
}
export default NewQuestion;