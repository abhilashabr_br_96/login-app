import React, { useState } from "react";
// import QuizeApp from "./QuizeAppComponent";
import './QuizeAppComponent.css';
import Card from "../UI/Card/Card";
import QuestionFilter from "./QuestionFilter";
import QuestionList from "./QuestionList";
import QuizeTimer from "./QuizeTimer";
import QuestionTotal from "./QuizeTotalQuestion";
// import QuizeChart from "./QuizeAppChart";
import QuizeAppChart from "./QuizeAppChart";
import Button from "../UI/Button/Button";
import {useNavigate} from 'react-router-dom'

// import Finalres
const QuestionandOption = (props) => {
    const [isShowFinalResult,setIsShowFinalResult]=useState(false);
    const [isShowResult, setIsShowResult] = useState(false);
    const [disable,setIsDisable]=useState(false);

    const [filteredCategory, setfilteredCategory] = useState('  ');
    const { questions } = props;

    const filterchangeHandler = (selectedCategory) => {
        setfilteredCategory(selectedCategory);
    };

    const fileteredCategory = questions.filter((question) => {
        return (question.category === filteredCategory)
    });

    const filteredQuestionLength = questions.filter((question) => {
        return (question.category === filteredCategory)
    });
    return (
        <React.Fragment >
            <Card className="question-card">
                <div>
                    <QuizeAppChart fileteredCategory={fileteredCategory} questions={questions} selectedFilteredQuestionLength={filteredQuestionLength} />
                    <QuestionTotal selectedFilteredQuestionLength={filteredQuestionLength}  />
                    {/* <QuizeTimer /> */}
                    <QuestionFilter
                        onStarttest={props.onStarttest}
                        questions={questions}
                        selectedFilteredQuestionLength={filteredQuestionLength}
                        selected={filteredCategory}
                        onFilterChange={filterchangeHandler}
                    />
                    <QuestionList 
                    optionValue={props.optionValue} 
                    correctAns={props.correctAns}
                    selectedOption={props.selectedOption}
                    selected={filteredCategory}
                    onCloseCart={props.onCloseCart}
                    disabled={props.disable}  
                    onsubmit={props.onsubmit} 
                    onCancel={props.onCancel} 
                    questions={fileteredCategory} 
                    selectedFilteredQuestionLength={filteredQuestionLength}
                    />
                </div>

               {console.log('optionValue :>> ', props.optionValue)}
                {/* <div className="show-Button">
                    {!isShowResult && <div><button onClick={startShowResultHandler}>Submit</button></div>}

                    {isShowResult && <ResultBox onCancel={stopShowResultHandler} />}

                    
                </div> */}

            </Card>
        </React.Fragment>
    )
}

export default QuestionandOption;