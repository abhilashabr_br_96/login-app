import React, { useState } from "react";
import './QuestionFilter.css';
import StartQuizeModel from "../UI/ErrorModal";
import QuizeModule from "../Cart/cart";
import QuizeTimer from "./QuizeTimer";
const QuestionFilter = (props) => {
    const [startQuize, setStartQuize] = useState(false);
    const [starttimer, setStartTimer] = useState(false);
    const [filteredcategory, setfilteredCategory] = useState('');
    const { selectedFilteredQuestionLength } = props
    const categoryHandler = (event) => {
       
        // setEnteredFilter({event.target.value})
        if (event.target.value.trim().length === 0) {
            setStartQuize(false);
            setStartTimer(false);
        }
        if (event.target.value.trim().length > 0) {
            setStartQuize(true);
        }
        props.onFilterChange(event.target.value)
    }

    const starttestHandler = () => {
        setStartTimer(true);
        setStartQuize(false);
    }

    const closeCartHandler = () => {
        setStartQuize(false);
        setStartTimer(false);

    }

    return (
        <div className="question-filter-container">
            {starttimer && <div className="starttimer"><QuizeTimer /></div>}
            {startQuize && (<div>
                <QuizeModule
                    questions={props.questions}
                    selected={props.selected}
                    onClick={props.onStarttest}
                    filteredlength={props.selectedFilteredQuestionLength}
                    onCloseCart={closeCartHandler}
                    question={props.questions}
                    starttest={starttestHandler}
                />
            </div>)}
            {console.log('props.selected >> ', props.selected.length)}
            <div className="questions-filter">
                <div className="questions-filter__control">
                    <label className="questions-filter label">Filter</label>

                    <select value={props.selectedCategory} onChange={categoryHandler}>
                        <option value=''>Select Category</option>
                        <option value="javascript">JavaScript</option>

                        <option value="css">CSS</option>

                        <option value="html">HTML</option>

                        <option value="react">React</option>

                        <option value="angular">Angular</option>

                    </select>

                </div>
            </div>
        </div>
    )
}
export default QuestionFilter;