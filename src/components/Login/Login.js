import React, { useEffect, useState } from 'react';
import '../../App.css';
import Card from '../UI/Card/Card';
import classes from './Login.module.css';
import Button from '../UI/Button/Button';
import ErrorModal from '../UI/ErrorModal';
// import image from '../../assets/image5.png';
const Login = (props) => {
  const [enteredEmail, setEnteredEmail] = useState('');
  const [emailIsValid, setEmailIsValid] = useState();
  const [enteredPassword, setEnteredPassword] = useState('');
  const [passwordIsValid, setPasswordIsValid] = useState();
  const [formIsValid, setFormIsValid] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    const timer = setTimeout(() => {
      setFormIsValid(
        enteredEmail.includes('@') && enteredEmail.includes('.') && enteredPassword.trim().length > 6
      );
    }, 500);
    return () => {
      clearTimeout(timer);
    }
  }, [enteredEmail, enteredPassword])


  const emailChangeHandler = (event) => {
    setEnteredEmail(event.target.value);

    // setFormIsValid(
    //   event.target.value.includes('@') && enteredPassword.trim().length > 6
    // );
  };

  const passwordChangeHandler = (event) => {
    setEnteredPassword(event.target.value);

    // setFormIsValid(
    //   event.target.value.trim().length > 6 && enteredEmail.includes('@')
    // );
  };

  const validateEmailHandler = () => {
    setEmailIsValid(enteredEmail.includes('@'));
  };

  const validatePasswordHandler = () => {
    setPasswordIsValid(enteredPassword.trim().length > 6);
  };

  const submitHandlerTrainer = (event) => {
    event.preventDefault();
    if (enteredEmail.trim().length === 0 || enteredPassword.trim().length === 0) {
      setError(
        {
          title: 'Invalid Input',
          message: 'Please enter the email id and password'
        }
      )
      return;
    }

    {
      props.users.map((user) => {
        if ((enteredEmail.trim() !== user.email) && (enteredPassword.trim() !== user.password)) {
          setError(
            {
              title: 'Invalid Input',
              message: 'Sorry😒!!!the entered email address is not Registered '
            }
          )
          return;
        }
      })
    }

    props.onLoginTrainer(enteredEmail, enteredPassword);

  };
  const errorModal = () => {
    setError(null);
  }

  return (
    <div>

      {error && (<ErrorModal title={error.title} message={error.message} onConfirm={errorModal} />)}
      <Card className={classes.login}>

        <form onSubmit={submitHandlerTrainer}>
          {/* image bump */}
          <div className=''>
            <h3>Login Here</h3>
            {/* <img className='' /> */}
          </div>
          <div
            className={`${classes.control} ${emailIsValid === false ? classes.invalid : ''
              }`}
          >
            <label htmlFor="email">E-Mail</label>
            <input
              type="email"
              id="email"
              value={enteredEmail}
              onChange={emailChangeHandler}
              onBlur={validateEmailHandler}
            />
          </div>
          <div
            className={`${classes.control} ${passwordIsValid === false ? classes.invalid : ''
              }`}
          >
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              value={enteredPassword}
              onChange={passwordChangeHandler}
              onBlur={validatePasswordHandler}
            />
          </div>
          <div className={classes.actions}>
            <Button type="submit" className={classes.btn} >
              Login
            </Button>
           
          {/* </div> */}
          {/* <div className={classes.actions}> */}
            <a href='/.RegisterForm.js' target='blank' >New User?</a>
          </div>
        </form>
        {/* <div className="App">
          <header className="App-header ">
            <img src={image} className="App-logo" alt="image" />
          </header>
        </div> */}
      </Card>
    </div>
  );
  // disabled={!formIsValid}
};

export default Login;
