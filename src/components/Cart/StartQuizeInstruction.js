import React, { useContext } from "react";
// import QuestionContext from "../../store/QuestionContext";
import classes from "./CartItem.module.css";
// import icon from "../../assets/deleteicon.png";
const StartQuizeInstruction = (props) => {

    return (

        <li className={classes["cart-item"]}>
            <div className={classes.divs}>

                <h1>Instructions</h1>
                <h5>You have to Select correct option </h5>
                <h5>15 minuts time for test </h5>
                <h5>You can see you wrong attempts in wrong attempts cart </h5>
            </div>
            <div><button onClick={props.onstarttest}>Start Quize</button></div>

        </li>
    );
};

export default StartQuizeInstruction;

