import React, { useContext } from "react";
import classes from './Cart.module.css';
import QuestionContext from "../../store/QuestionContext";
import QuizeResultModel from "../UI/QuizeResultModel";

const ResultQuizeModule = (props) => {
    const crtCtx=useContext(QuestionContext);
    let score=0; 
    score=props.selectedFilteredQuestionLength.length-crtCtx.questions.length
    const scorepre=score / props.selectedFilteredQuestionLength.length * 100
    const score1 =(scorepre >=85)&&(scorepre <=100 ) ;
    const score2 =(scorepre >=70)&&(scorepre <=85 );
    const score3 =(scorepre >=50)&&(scorepre <=65 );
    const score4 =scorepre <=40 ;
   
    return (
        <QuizeResultModel onClick={props.onCloseCart} selected={props.selected}>
            <footer className={classes.actions}>

                {<div className="actions-btn">
                <div className={classes.head}>
                    
                    { score1? <h5>Congratulations</h5> : null }
                    { score2 ? <h5>Excellent job</h5> : null }
                    { score3 ? <h5>Good</h5> : null }
                    { score4 ? <h5>Sorry you did not pass</h5> : null }
                    
                    <div> <h5> The selected Course is: {props.selected}</h5></div>
                  
                    <div> <h3>You scored {score} out of {props.selectedFilteredQuestionLength.length} correct - ({score / props.selectedFilteredQuestionLength.length * 100}%)</h3></div>
                    </div>
                </div>}
                {<button className={classes.button} onClick={props.starttest}>Start Quize</button>}
                <button className={classes['button--alt']} onClick={props.onCloseCart}>Close</button>

            </footer>

        </QuizeResultModel>
    )
}
export default ResultQuizeModule;


