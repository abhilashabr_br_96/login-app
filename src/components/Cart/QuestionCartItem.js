import React, { useContext } from "react";
import QuestionContext from "../../store/QuestionContext";
import classes from "./CartItem.module.css";
import icon from "../../assets/deleteicon.png";

const CartQuestion = (props) => {

    return (

        <li className={classes["cart-item"]}>
            <div className={classes.divs}>

                <div className={classes.optionwrap}>
                    <h4 key={props.id}>{props.index + 1}.{props.name}</h4>
                    {[props.option1,
                    props.option2,
                    props.option3,
                    props.option4
                    ].map((option) => {
                        console.log('option :>> ', option);
                        console.log('props.selectedOption :>> ', props.selectedOption);
                        console.log('props.correctAns :>> ', props.correctAns);

                        return (
                            <p key={option}
                                className={`${classes.list} ${props.correctAns === option ? classes.green : ''}
                                ${props.selectedOption === option ? classes.red : ''}`}
                            >{option}</p>

                        )
                    })

                    }

                    <div className={classes.delete}>
                        <img src={icon} onClick={props.onRemove} />
                       
                    </div>
                </div>
            </div>

        </li>
    );
};

export default CartQuestion;

