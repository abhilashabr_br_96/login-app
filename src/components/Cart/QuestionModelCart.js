import React, { useContext, useState } from "react";
import classes from './Cart.module.css';
// import Button from "../ui/Button";
import Model from "../UI/Model/Model";
import QuestionContext from "../../store/QuestionContext";
import CartQuestion from "./QuestionCartItem";
import Card from "../UI/Card/Card";


const CartModule = (props) => {
    const crtquestion = useContext(QuestionContext);
    const quetionsrem = crtquestion.questions;
    const hashItem = crtquestion.questions.length > 0;
    const totalQuestionNumber = `${crtquestion.totalQuestionNumber}`;
    // const hashItem=crtquestion.items.length>0;
    const CaetItemRemoveHandler = (id) => {
        crtquestion.removeQuestion(id);
    }

    const clearAllQuestionHandler = () => {
        crtquestion.questions.map((question)=>{
            crtquestion.removeQuestion(question.id)
        })
    }

    {
        crtquestion.questions.map((question) => {
            console.log('question.map :>> ', question.text);
            console.log('option1 :>> ', question.option1);
        })
    }

    const cartList = (
        <ul className={classes["cart-items"]}>
            {crtquestion.questions.map((question, index) => (

                <CartQuestion
                    index={index}
                    key={question.id}
                    name={question.text}
                    option1={question.option1}
                    option2={question.option2}
                    option3={question.option3}
                    option4={question.option4}
                    correctAns={question.correctAns}
                    selectedOption={question.selectedOption}
                    onRemove={CaetItemRemoveHandler.bind(null, question.id)}
                />
            )
            )

            }
        </ul>
    );
    return (
        <Model onClick={props.onCloseCart}>
            {cartList}
            <footer className={classes.actions}>
                {!hashItem && <div className="actions-btn">
                    <h5>Congratulations!!!😀</h5>
                    <div> <h6>You have no Wrong Attempts.All the very best</h6></div>
                </div>}
               {hashItem&&
                 <div className={classes.checkbox}>
                 <div className={classes.checkboxgreen}>
                     <input  checked readOnly type='checkbox' />Correct Answer
                 </div>
                 <div className={classes.checkboxred}>
                     <input className={classes.checkboxgred} checked readOnly type='checkbox' />Wrong Answer
                 </div>
             </div>
               }
                <button className={classes['button--alt']} onClick={props.onCloseCart}>Close</button>
                {hashItem && <button className={classes.button} onClick={clearAllQuestionHandler}>Clear</button>}
            </footer>
            {/* {hashItem&&<button></button>} */}
        </Model>
    )
}
export default CartModule;


