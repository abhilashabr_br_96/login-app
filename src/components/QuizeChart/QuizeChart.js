import React from "react";
import './QuizeChart.css';
// import ChartBar from "./ChartBar";
import QuizeChartBar from "./QuizeChartBar";
const QuizeChart = (props) => {
  const dataPointValues = props.dataPoints.map((dataPoint) => dataPoint.value);
  const totalMaximum = Math.max(...dataPointValues);
  return (
    <div className="chart">
      {props.dataPoints.map((dataPoint) => {
        return (
          <QuizeChartBar

            key={dataPoint.label}
            value={dataPoint.value}
            maxValue={totalMaximum}
            label={dataPoint.label}
          />
        )
      })}
    </div>
  );
};

export default QuizeChart;