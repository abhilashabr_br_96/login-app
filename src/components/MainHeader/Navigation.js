import React, { useContext } from 'react';
import QuestionContext from '../../store/QuestionContext';
import classes from './Navigation.module.css';
import {useNavigate} from 'react-router-dom'
import {NavLink} from 'react-router-dom'

const Navigation = (props) => {
  const cartCtx=useContext(QuestionContext);
  // const numberOfCartQuestions=cartCtx.questions.reduce((currentValue,question)=>{
  //   return  currentValue + question.totalQuestionNumber
  // },0)
const navigate=useNavigate();
  const numberOfCartQuestions = cartCtx.questions.length
  return (
    <nav className={classes.nav}>
      <ul className={classes.ulcolor}>
    
        {(!props.isLoggedInTrainee && !props.isLoggedInTrainer) && !props.isLoggedInAdmin && (
          <button onClick={(props.onSignUpTrainer)}>SignUp
          </button>
        )}
   
        {/* <NavLink to='/signup'>Sign Up</NavLink> */}
    

        {props.isLoggedInTrainee && (
          <div className='btntrainee'>
            {props.isShowAttempt&& <button onClick={props.onShowCart}>
              <span className={classes.badge}>Wrong Attempts</span>
              <span className={classes.bump}>{numberOfCartQuestions}</span>
            </button>}
           
            <button onClick={props.onLogoutTrainee}>Logout
            </button>

          </div>
        )}
        


        {props.isLoggedInTrainer && (
          <button onClick={props.onLogoutTrainer}>Logout
          </button>
        )}


        {props.isLoggedInAdmin && (
          <button onClick={props.onLogoutAdmin}>Logout</button>
        )}

      </ul>
    </nav>
  );
};

export default Navigation;


