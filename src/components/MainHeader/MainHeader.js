import React from 'react';
// import mealsImage from "../../assets/image.jpg";
import Navigation from './Navigation';
import classes from './MainHeader.module.css';
// import HeaderCartButton from './HeaderCartButton';

const MainHeader = (props) => {


  return (
    <React.Fragment>
      <header className={classes['main-header']}>
      <h1>Quize App</h1>   
        <Navigation 
         isShowAttempt={props.isShowAttempt}
          onShowCart={props.onShowCart}
          onSignUpTrainer={props.onSignUpTrainer}
          isLoggedInTrainer={props.isAuthenticatedTrainer}
          onLogoutTrainer={props.onLogoutTrainer}
          isLoggedInTrainee={props.isAuthenticatedTrainee}
          onLogoutTrainee={props.onLogoutTrainee}
          isLoggedInAdmin={props.isAuthenticatedAdmin}
          onLogoutAdmin={props.onLogoutAdmin}
        />
      </header>
      
    </React.Fragment>

  );
};

export default MainHeader;

