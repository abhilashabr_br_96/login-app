import React, { useState, useRef, useEffect } from "react";
import classes from './RegisterForm.module.css';
import Button from "../UI/Button/Button";
import Card from '../UI/Card/Card';
import ErrorModal from "../UI/ErrorModal";
import SuccessModal from "../UI/SuccessModal";
import {useNavigate} from 'react-router-dom';
const AddUser = (props) => {
    // const [formIsValid,setformIsValid]=useState(false);
    const navigate=useNavigate();
    const userNameRef = useRef();
    const emailRef = useRef();
    const passwordRef = useRef();
    const confirmPswdRef = useRef();
    const phoneNumberRef = useRef();
    // const TrainerRef=useRef();
    // const TraineeRef=useRef();
    const roleRef = useRef();
    // const [enteredRole, setenteredrole] = useState('');
    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');
    //  const [formIsValid,setFormIsValid]=useState(false);


    // const enteredRoleHandler = (event) => {
    //     if (event.target.value.trim.length === 0) {
    //         setenteredrole(event.target.value);
    //     }
    // }

    
    const onAddRegisterHandler = (event) => {
        const entereduserName = userNameRef.current.value;
        const enteredEmail = emailRef.current.value;
        const enteredPassword = passwordRef.current.value;
        const enteredConfirmPassword = confirmPswdRef.current.value;
        const enteredPhoneNumber = phoneNumberRef.current.value;
        const enteredRole = roleRef.current.value;
        // const enteredTrainer=TrainerRef.current.value;
        // const enteredTrainee=TraineeRef.current.value;

        event.preventDefault();
        props.users.map((user) => {
            if ((enteredEmail.trim() === user.email) && (enteredPassword.trim() === user.password)) {
                // setError(
                //     {
                //         title: 'Invalid Input',
                //         message: 'Sorry😒!!!The entered email address is already Registered '
                //     }
                // )
                //  return;
                alert('Email and Password already registered')
            }
        });
        
        if (enteredRole.trim().length === 0) {
            setError({
                title: 'Invalid Input',
                message: 'Please enter your Role'
            });
            return;
        }

        if (entereduserName.trim().length === 0) {
            setError({
                title: 'Invalid Input',
                message: 'Please enter the username'
            });
            return;
        }
        if (enteredEmail.trim().length === 0 && !enteredEmail.includes('@') && !enteredEmail.includes('.')) {
            setError({
                title: 'Invalid Input',
                message: 'Please enter the email'
            });
            return;
        }
        if (enteredPassword.trim().length === 0 || enteredPassword.trim().length <= 6) {
            setError({
                title: 'Invalid Input',
                message: 'Please enter atleast 6 characters'
            });
            return;
        }
        if (enteredConfirmPassword.trim().length === 0 || enteredConfirmPassword !== enteredPassword) {
            setError({
                title: 'Invalid Password',
                message: 'Please enter correct Password'
            });
            return;
        }
        if (enteredPhoneNumber.trim().length === 0 || enteredPhoneNumber.trim().length !== 10) {
            setError({
                title: 'Invalid Input',
                message: 'Please enter 10 digit number    '
            });
            return;
        }
        if (enteredRole.trim().length === 0) {
            setError({
                title: 'Invalid Input',
                message: 'Please Enter your Role'
            });
            return;
        }

    
        const newRegister = {
            id: Math.random().toString(),
            userName: entereduserName,
            email: enteredEmail,
            password: enteredPassword,
            confirmPassword: enteredConfirmPassword,
            phoneNumber: enteredPhoneNumber,
            role: enteredRole,
            // role:[enteredTrainer,enteredTrainee]
        }

        props.onAddUsers(newRegister);
        userNameRef.current.value = ('');
        emailRef.current.value = ('');
        passwordRef.current.value = ('');
        confirmPswdRef.current.value = ('');
        phoneNumberRef.current.value = ('');
        //  TraineeRef.current.value=('');
        //  TrainerRef.current.value=('');
        roleRef.current.value = ('');
        // setenteredrole('');

        setSuccess({
            title: 'Sussess ',
            message: 'You are Registered Successfully!!!✌'
        });
        return;
    }

    // const cancelHandler = () => {
    //     props.onCancel
    // }

    const errorModal = () => {
        setError(null);
    }
    const successModal = () => {
        setSuccess(null);
    }

    return (
        <div>
            {success && (<SuccessModal title={success.title} message={success.message} onConfirm={successModal} />)}
            {error && (<ErrorModal title={error.title} message={error.message} onConfirm={errorModal} />)}
            {/* image1 bump1 */}
            {/* <div className=''>
                <h3>Register New user </h3>
                <img className='' />
            </div> */}
            <Card className={classes.input}>

                <form onSubmit={onAddRegisterHandler}>
                    <h3>Register New user </h3>
                    <div>
                        <label htmlFor="role">Role</label>
                        <input id="role" readOnly type='text' ref={roleRef} value='Trainee' />
                        {/* <select type='text' onChange={enteredRoleHandler}>
                            <option value=''>Select the Role</option>
                            <option value='trainer'>Trainer</option>
                            <option value='trainee'>Trainee</option>
                        </select> */}
                    </div>
                    <div>
                        <label htmlFor="userName">User Name</label>
                        <input id="userName" type='text' ref={userNameRef} />
                    </div>
                    <div>
                        <label htmlFor="email">Email Id</label>
                        <input id="email" type='email' ref={emailRef} />
                    </div>
                    <div>
                        <label htmlFor="password">Password</label>
                        <input id="password" type='password' ref={passwordRef} />
                    </div>
                    <div>
                        <label htmlFor="confirmPswd">Confirm Password</label>
                        <input id="confirmPswd" type='password' ref={confirmPswdRef} />
                    </div>
                    <div>
                        <label htmlFor="phoneno">Phone Number</label>
                        <input id="phoneno" type='text' ref={phoneNumberRef} />
                    </div>

                    <div className={classes.buttons}>
                        <Button type="submit" className={classes.btn} >
                            Register
                        </Button>
                        {/* <Button type="cancel" onClick={()=>navigate(-1)} className={classes.btn} >
                            Cancel
                        </Button> */}
                                
                        {!props.isRegister && (
                            <Button type='cancel' className={classes.btn} onClick={props.onCancel}>Cancel
                            </Button>
                        )}
                    </div>
                </form>

            </Card>
        </div>
    )
}

export default AddUser;


// disabled={!formIsValid