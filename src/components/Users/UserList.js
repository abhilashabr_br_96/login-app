import React, { useState } from "react";
import classes from './UserList.module.css';
import Card from '../UI/Card/Card';
const UserList = (props) => {
    const [search, setSearch] = useState('');
    const { users } = props;

    const searchHandler = (event) => {
        setSearch(event.target.value);
        console.log('event.target.value :>> ', event.target.value);
    }


    const data = {
        nodes: users.filter((user) =>
            user.userName.toLowerCase().includes(search.toLowerCase())
        )
        
    };

    console.log('data :>> ', data);

    return (
        <Card className={classes.users}>
            <h2>User List</h2>
            <div>
                <label htmlFor="search">
                    Search by Role:
                    <input id="search" type='text' onChange={searchHandler} />
                </label>
            </div>

            <div>
                <table border="1" >
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Confirm Password</th>
                            <th>Phone Number</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody data={data}>
                        {props.users.map((user) => {
                            return <tr>
                                <td>{user.userName}</td>
                                <td>{user.email} </td>
                                <td>{user.password} </td>
                                <td>{user.confirmPassword} </td>
                                <td>{user.phoneNumber}</td>
                                <td>{user.role}</td>

                            </tr>
                        })}

                    </tbody>
                </table>
            </div>

        </Card>
    )
}

export default UserList;